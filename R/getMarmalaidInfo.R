#' Get Maramalaid sample annotation data frame
#'
#' @return Maramalaid sample annotation data frame
#' @export
#'
getMarmalaidSampleInfo <- function() {
  marmalaidAnnotation
}

#' Get Maramalaid probe list
#'
#' @return Maramalaid probe list
#' @export
#'
getMarmalaidProbeList <- function() {
  marmalaidProbes
}
