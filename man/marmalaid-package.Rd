\name{marmalaid-package}
\alias{marmalaid-package}
\alias{marmalaid}
\docType{package}
\title{
What the package does (short line)
MARMAL-AID
}
\description{
Database of publicly available 450K datasets easily accessible from R. This package contains all of the publicly available data and is a large downloadable size.
}
\details{
\tabular{ll}{
Package: \tab marmalaid\cr
Type: \tab Package\cr
Version: \tab 1.0\cr
Date: \tab 2013-02-14\cr
License: \tab GPLv3\cr
}
	Use getbeta to obtain matrix of data for particular probes and/or samples.

}
\author{
	Robert Lowe

Maintainer: Robert Lowe <r.lowe@qmul.ac.uk>
}
\references{

}
